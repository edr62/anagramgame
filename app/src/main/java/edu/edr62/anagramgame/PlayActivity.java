package edu.edr62.anagramgame;

import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Main play activity of the anagram game
 */
public class PlayActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView wordTv;
    private EditText wordEnteredTv;
    private Button validate, newGame;
    private String wordToFind;
    public int success_count = 0;

    Chronometer simpleChronometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        wordTv = (TextView) findViewById(R.id.wordTv);
        wordEnteredTv = (EditText) findViewById(R.id.wordEnteredEt);
        validate = (Button) findViewById(R.id.validate);
        validate.setOnClickListener(this);
        newGame = (Button) findViewById(R.id.newGame);
        newGame.setOnClickListener(this);
        simpleChronometer = (Chronometer) findViewById(R.id.Chronometer);
        simpleChronometer.setBase(SystemClock.elapsedRealtime());
        simpleChronometer.start();

        newGame();
    }

    @Override
    public void onClick(View view) {
        if (view == validate) {
            validate();
        } else if (view == newGame) {
            newGame();
        }
    }

    private void validate() {
        String w = wordEnteredTv.getText().toString();

        if (wordToFind.equals(w)) {
            success_count++;
            Toast congrat_toast= Toast.makeText(this,
                    "Congratulations! You found the word: \n" + wordToFind + ".\n\nSuccessful Guesses: " + success_count, Toast.LENGTH_LONG);
            congrat_toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 0);
            congrat_toast.show();
            newGame();
        } else {
            Toast retry_toast= Toast.makeText(this,
                    "Please try again!", Toast.LENGTH_SHORT);
            retry_toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 0);
            retry_toast.show();
        }
    }

    private void newGame() {
        wordToFind = Anagram.randomWord();
        String wordShuffled = Anagram.shuffleWord(wordToFind);
        wordTv.setText(wordShuffled);
        wordEnteredTv.setText("");
        long tStart = System.currentTimeMillis();
    }

}